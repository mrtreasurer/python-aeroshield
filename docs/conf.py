# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import os
import sys
sys.path.insert(0, os.path.abspath('../src/automationshield/'))

# -- Project information -----------------------------------------------------

project = u"python-automationshield"
copyright = u"2024, Bert Van den Abbeele"
author = u"Bert Van den Abbeele"

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "myst_nb",
    "sphinx.ext.autodoc",
    "sphinx.ext.viewcode",
    "sphinx.ext.intersphinx",
    "sphinx_copybutton",
    "matplotlib.sphinxext.plot_directive"
]

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ["_build", "Thumbs.db", ".DS_Store"]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = "furo"


# ----------------------------------------------------------------------------

# do not execute notebooks
nb_execution_mode = "off"
# number lines in examples
nb_number_source_lines = True

intersphinx_mapping = {
    'python': ('https://docs.python.org/3.10/', None),
    'numpy': ('https://numpy.org/doc/stable/', None),
    'matplotlib': ('https://matplotlib.org/stable/', None),
    'pyserial': ('https://pythonhosted.org/pyserial', None),
}


# autoapi_options = ["members", "undoc-members", "show-inheritance", "special-members", "imported-members"]

autoclass_content = "both"
autodoc_member_order = "bysource"
autodoc_default_options = {
    "members": None,
    "undoc-members": None,
    "show-inheritance": None
}

autodoc_typehints = "both"

mathjax3_config = {
    "loader": {"load": ["[tex]/gensymb"]},
    "tex": {"packages": {"[+]": ["gensymb"]}}
}
