BaseShield Class
================

.. autoclass:: automationshield.shields.BaseShield
    :special-members: __enter__, __exit__
    :private-members: _read, _write