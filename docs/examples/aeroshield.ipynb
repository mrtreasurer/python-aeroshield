{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Getting Started with a Shield Class\n",
    "\n",
    "Before we start:\n",
    "* This tutorial is written for the AeroShield, but applies to all shield classes.\n",
    "* It should be noted that you won't need to use the `AeroShield` class (or other Shield classes) directly for most purposes. Often, it will be more convenient to use the `ShieldController` class. `AeroShield` takes care of communication with the Arduino, but doesn't implement anything else that would be needed for a controller, e.g. a configurable time step, data logging, ... All of this is available in the `ShieldController` (See the [ShieldController example](./controller.ipynb)). If you're sure you need to use the `AeroShield` class, read on.\n",
    "\n",
    "First, we import the `AeroShield` class from the `automationshield` package."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {},
   "outputs": [],
   "source": [
    "from automationshield import AeroShield"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using the `with` Context Manager\n",
    "\n",
    "`AeroShield` supports the `with` context manager, which takes care of opening and closing the serial connection, calibrating the angle reading at the start and making sure the fan is stopped at the end. Using the context manager is preferred, since it guarantees the fan will stop, even if an error occurs at some point. In the code block below, the motor power is controlled by the value of the potentiometer on the shield."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [],
   "source": [
    "with AeroShield() as aero_shield:\n",
    "    # make an initial write to the arduino so it will send something back we can act on.\n",
    "    aero_shield.write(flag=aero_shield.RUN, actuator=0)\n",
    "\n",
    "    for _ in range(1000):\n",
    "        # read state\n",
    "        pot, angle = aero_shield.read()\n",
    "\n",
    "        # act on state\n",
    "        motor = pot\n",
    "\n",
    "        # write motor input\n",
    "        aero_shield.write(flag=aero_shield.RUN, actuator=motor)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's break down what happens here:\n",
    "\n",
    "* We create an instance of `AeroShield`, which we call `aero_shield`. The context manager will also calibrate the angle measurement. Calibration is done by reading and storing the measurement value at the start of the experiment. This means the pendulum should be at rest before starting an experiment.\n",
    "* We write to the shield once before we enter the `for` loop. Why do we do this? The communication with the Arduino is set up such that when it gets an input from the computer, it will return the state of the device once, and wait for a new input before it sends the state again. That initial write to the Arduino ensures that we can read the state at the start of our loop.\n",
    "* The motor power should be provided to the `AeroShield.write` method as a percentage [0, 100]. Since the potentiometer value is provided in percentage, we don't need to do any scaling.\n",
    "* We loop 1000 times through reading the Aeroshield and writing a command to the Aeroshield. It's important to note that the frequency of the loop is not in any way controlled in this script. If you need a fixed time step, you need to implement that yourself, or use the `ShieldController`, which does that for you.\n",
    "* We read the state of the Aeroshield. The return of `read` is the potentiomenter value (\\%) and the angle measurement (°).\n",
    "* We do something with the state. This is where you would implement any control algorithm. In the example, we simply set the motor value equal to the potentiometer.\n",
    "* We write the motor value to the Arduino. The `flag` parameter takes either `AeroShield.RUN`, as is the case here, or `AeroShield.STOP`. Use `RUN` during your experiment. The `STOP` flag will stop the fan, regardless of the motor value given.\n",
    "* When the for loop is finished, the context manager exits, ensuring that the fan is stopped and the serial connection is closed.\n",
    "\n",
    "The runtime of this script is dependent on your system and the specific Arduino, but the frequency has been recorded to get close to 1 kHz."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `AeroShield` without the Context Manager\n",
    "\n",
    "If you don't want to or can't use the context manager, the script below does the same thing as the one above, without the context manager. The first three commands create an `AeroShield` instance, open the connection and calibrate the angle measurement. This was all done by the context manager in the above example. The loop is identical. At the end, the `stop` method sends the `AeroShield.STOP` flag to the Arduino, stopping the fan. The `close` method closes the serial connection."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "aero_shield = AeroShield()\n",
    "\n",
    "# Open the serial connection\n",
    "aero_shield.open()\n",
    "# Calibrate the pendulum angle at rest\n",
    "aero_shield.calibrate()\n",
    "\n",
    "aero_shield.write(flag=aero_shield.RUN, actuator=0)\n",
    "\n",
    "for _ in range(1000):\n",
    "    # read state\n",
    "    pot, angle = aero_shield.read()\n",
    "\n",
    "    # act on state\n",
    "    motor = pot\n",
    "\n",
    "    # write motor input\n",
    "    aero_shield.write(flag=aero_shield.RUN, actuator=motor)\n",
    "\n",
    "# Tell the Arduino to stop the fan\n",
    "aero_shield.stop()\n",
    "# Close the serial connection\n",
    "aero_shield.close()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Manually Setting the Port\n",
    "\n",
    "In most cases, the `AeroShield` class will find which port the Arduino is connected to. It will raise an exception when it fails to do so. When that happens, you need to find out the port using your operating system's tools and provide it like so: "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [],
   "source": [
    "aero_shield = AeroShield(port=\"COM20\")"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.13"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
