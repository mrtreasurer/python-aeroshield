# Changelog

<!--next-version-placeholder-->

## v1.2.0 (06/11/2024)

### Features

- Add `StateSpaceShield` class, allowing to simulate state-space systems within the framework of this package.
- Add `AeroShieldMimic`, a class implementing a state-space system that simulates the AeroShield device.

### Fixes
- Reduce exception rate thrown by `LivePlotter` on KeyboardInterrupt.

### Docs
- Moved away from sphinx-autoapi to autodoc. Reorganised documentation.

## v1.1.4

### Fix
- Cap LivePlotter frame rate and max(60, experiment frequency) Hz
- Fix graceful exit: catch keyboardinterrupt in main process and return available data so script can continue
- Fix arduino compilation: compile in CI, include hex files in package

## v1.1.3

### Docs
- Update plotting example in readme to match updates

## v1.1.2

### Fix
- Add another missing dependency for rtd because I'm stupid

## v1.1.1

### Fix
- Add missing dependency to build docs in rtd

## v1.1.0

### Features
- Rebuilt Plotters to enable functionality across windows and linux
- Added option to get reference through callback function in shieldcontroller.run

### Changes
- Changed experiment loop in shieldcontroller.run such that first cycle has t=0

### Docs
- Changed theme to furo

## v1.0.3

### Bug fixes
- Remove __version__, it's annoying
- Add print statements to install_firmware method

## v1.0.2

### Bug fixes
- Fix __init__() signature of MagnetoShield to match BaseShield: remove baudrate

## v1.0.1

### Bug fixes
- Fix version checking in __init__.py to check "python_automationshield"

## v1.0.0

### Features
- Flash Arduino firmware from shield class using arduino-cli
- Add method to convert MagnetoShield flux density reading to an estimated distance of the permanent magnet from the electromagnet

### Docs
- Expanded documentation on shields, controller and arduino


## v0.4.0

### Changes
- Rename package, repository to python-automationshield
- Rename AeroController to ShieldController
- Actuator value now needs to be supplied to BaseShield.write as percentage by default

### Features
- Create base class BaseShield
- Add FloatShield class
- Add MagnetoShield class
- Add arduino script for floatshield
- Add arduino script for magnetoshield

### Docs
- update readme, changelog, contributing, etc. to new name
- update controller example: new constants for PID controller for actuator in percentage

## v0.3.3

### Fix
- Fix calibration bug: a second calibration of the angle sensor caused an incorrect `zero_angle`.

## v0.3.2

### Fix
- Open serial connection before attempting calibration and resetting buffers

### Docs
- Remove Gitlab pages, only rtd now

## v0.3.1

### Docs
- Change documentation link in Readme to point to ReadTheDocs

### Deployment
- Added documentation and repository link to pyproject.toml to appear on pypi

### Changes
- Update Plotter and LivePlotter to include potentiometer value

## v0.3.0

### Docs
- Extend AeroShield example
- Extend AeroController example
- Publish documentation on GitLab pages, ReadTheDocs

### Changes
- Add potentiometer to default tracked variables of controller

## v0.2.0

### Docs
- python_aeroshield -> python-aeroshield
- Update README with basic usage examples.
- Update example notebooks.

### Changes
- Update AeroShield to be used standalone. No changes for user when using context manager.
- Rename plotting.Plotter.plot_hist to plotting.Plotter.plot

## v0.1.2

### Feature

- Track additional variables during run
- Move variable declaration out of `Controller.__init__`

## v0.1.1 (08/01/2024)

### Fix

- Bugs in package import

## v0.1.0 (04/01/2024)

- First release of `python_aeroshield`
