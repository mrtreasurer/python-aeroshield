#include <aeroshield.h>

AeroShield shield;

void setup() {
  shield.setup();
}

void loop() {
  shield.loop();
}